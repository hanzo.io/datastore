package main

import (
	"fmt"
	"net/http"

	"hanzo.io/datastore"
)

// Example entity
type Person struct {
	Name string
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Create a new instance
		ctx := r.Context()
		db := datastore.New(ctx)

		// Create a new entity
		p := &Person{"Nick"}

		// Write new entity to datastore
		key, err := db.Put("person", p)
		if err != nil {
			fmt.Fprintf(w, "Unable to write entity: %#v", err)
		}
		fmt.Fprintf(w, "Put %#v, key: %#v", p, key)
	})
}
