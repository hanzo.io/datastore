# datastore

Hanzo's `datastore` package enables simple access to Google Datastore.

## Install
Install using `go get`:

```bash
$ go get hanzo.io/datastore
```

## Usage

```go
package main

import (
    "net/http"

    "hanzo.io/datastore"
)

// Example entity
type Person struct {
    Name string
}

func main() {
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        // Create a new instance
        ctx := r.Context()
        db := datastore.New(ctx)

        // Write new entity to datastore
        key, err := db.Put("person", &Person{"Nick"})
    })
}
```

## License
[BSD](https://gitlab.com/hanzo.io/datastore/blob/master/LICENSE)
